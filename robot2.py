#!/usr/bin/env python3.5

import asyncio
from aiohttp import web
from rrb3 import *
import sys, os
import RPi.GPIO as GPIO
import atexit
import _thread
import time

rr = RRB3(12, 6)
if(sys.argv[0] == ''):
  indexPath = "static/index.html"
else:
  indexPath = os.path.dirname(sys.argv[0]) + "/static/index.html"
with open(indexPath, 'r') as file:
  indexContent = file.read()
GPIO.setmode(GPIO.BCM)
GPIO.setup(27, GPIO.OUT)
GPIO.output(27, True)

@asyncio.coroutine
def index(request):
  return web.Response(text=indexContent, headers={('Content-Type', 'text/html')})

@asyncio.coroutine
def forward(request):
  print('attempt forward')
  if 'time' in request.match_info:
    time = get_time(request)
    _thread.start_new_thread(lambda : rr.forward(time, 0.5), ())
  else:
    rr.forward(0, 0.5)
  print('forward complete')
  return web.Response(text='forward')

@asyncio.coroutine
def left(request):
  print('attempt left')
  if 'time' in request.match_info:
    time = get_time(request)
    _thread.start_new_thread(lambda: rr.left(time), ())
  else:
    rr.left()
  print('left complete')
  return web.Response(text='left')

@asyncio.coroutine
def right(request):
  print('attempt right')
  if 'time' in request.match_info:
    time = get_time(request)
    _thread.start_new_thread(lambda: rr.right(time), ())
  else:
    rr.right()
  print('right complete')
  return web.Response(text='right')

@asyncio.coroutine
def stop(request):
  print('attempt stop')
  rr.stop()
  print('stop complete')
  return web.Response(text='stop')

@asyncio.coroutine
def blink(request):
  print('attempt blink')
  if 'time' in request.match_info:
    time = get_time(request)
  else:
    time = 1
  _thread.start_new_thread(lambda: blink_internal(time), ())
  print('blink complete')
  return web.Response(text='blink')

def blink_internal(seconds):
  GPIO.output(27, False)
  time.sleep(seconds)
  GPIO.output(27, True)

def on_exit():
  print('Exiting...')
  GPIO.output(27, False)
  GPIO.cleanup()

def get_time(request):
  return (float)(request.match_info['time'])

app = web.Application()

app.router.add_get('/forward', forward)
app.router.add_get('/forward/{time}', forward)
app.router.add_get('/stop', stop)
app.router.add_get('/left', left)
app.router.add_get('/left/{time}', left)
app.router.add_get('/right', right)
app.router.add_get('/right/{time}', right)
app.router.add_get('/blink', blink)
app.router.add_get('/blink/{time}', blink)

app.router.add_get('/', index)

atexit.register(on_exit)

web.run_app(app, port=80)

